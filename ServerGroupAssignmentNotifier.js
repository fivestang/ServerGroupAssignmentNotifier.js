registerPlugin({
    name: 'ServerGroup Assignment Notifier',
    version: '1.1',
    description: 'Play an alert/sound/song when a ServerGroup is assigned.',
    author: 'fivestang <fivestang@protonmail.com>',
    autorun: false,
    vars: [{
        name: 'triggers',
        type: 'array',
        title: 'Add ServerGroup to watch for:',
        vars: [{
            name: 'servergroup',
            indent: 0,
            title: 'Which ServerGroup ID number should this audio play for?',
            type: 'number'
        },{
            name: 'audioalert',
            indent: 0,
            title: 'Which track should play for this ServerGroup?',
            type: 'track'
        }]
    }]
},
function(sinusbot, config) {
    var event = require('event')
    var media = require('media')
    var engine = require('engine')

    event.on('serverGroupAdded', function(ev) {
        for(i = 0; i < config.triggers.length; i++) {
            if (ev.serverGroup.id() == config.triggers[i].servergroup) {
                media.playURL(config.triggers[i].audioalert.url)
                engine.log("ServerGroup " + config.triggers[i].servergroup + " assigned, playing: " + config.triggers[i].audioalert.title)
            }
        }
    })
})
